#lang racket

; Loan Amortization
; May 12, 2009

; My daughter, a freshman in high school, is just completing her first programming class,
; using Java. Her final assignment was to write a program to print a loan amortization
; table, given an initial balance, annual interest rate, term in months, and monthly payment.

; Your task is to write that program (you are not restricted to Java), then print the
; amortization table for a three-year car loan of $10,000 at 7% (it’s an old textbook)
; When you are finished, you are welcome to read a suggested solution, or to post your
; solution or discuss the exercise in the comments below.
